terraform {
  backend "s3" {
    bucket = "firstbucketvpc"
    key    = "vpc/prod/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-locking"
  }
}
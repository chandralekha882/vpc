resource "aws_vpc_peering_connection" "peering" {
  peer_vpc_id   = data.aws_vpc.management.id
  vpc_id        = aws_vpc.custom_vpc.id
  auto_accept   = true

  tags = {
    Name = "Peering between management and VPC "
  }
}

resource "aws_route" "public" {
  route_table_id            = aws_route_table.public_rt.id
  destination_cidr_block    = data.aws_vpc.management.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.peering.id
  depends_on                = ["aws_route_table.public_rt"]
}

resource "aws_route" "private" {
  route_table_id            = aws_route_table.private_rt.id
  destination_cidr_block    = data.aws_vpc.management.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.peering.id
  depends_on                = ["aws_route_table.private_rt"]
}

resource "aws_route" "r" {
  count                     = length(tolist(data.aws_route_tables.mgmt.ids))
  route_table_id            = element(tolist(data.aws_route_tables.mgmt.ids),count.index)
  destination_cidr_block    = var.CIDR_BLOCK
  vpc_peering_connection_id = aws_vpc_peering_connection.peering.id
}
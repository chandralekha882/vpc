resource "aws_vpc" "custom_vpc" {
  cidr_block       = var.CIDR_BLOCK

  tags = {
    Name = "custom_vpc"
  }
}
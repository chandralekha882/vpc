resource "aws_subnet" "public_subnets" {
  count = length(data.aws_availability_zones.available.names)
  vpc_id     = aws_vpc.custom_vpc.id
  cidr_block = cidrsubnet(var.CIDR_BLOCK, 8, count.index )
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "public-subnet-${count.index}"
  }
}

resource "aws_subnet" "private_subnets" {
  count = length(data.aws_availability_zones.available.names)
  vpc_id     = aws_vpc.custom_vpc.id
  cidr_block = cidrsubnet(var.CIDR_BLOCK, 8, count.index+length(data.aws_availability_zones.available.names))
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "private-subnet-${count.index}"
  }
}